import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { LoginComponent } from './pages/login/login.component';
import { LoginGuard } from './guards/login.guard';
import { VendorComponent } from './pages/vendor/vendor.component';
import { EditVendorComponent } from './pages/vendor/edit-vendor/edit-vendor.component';
import { UsersComponent } from './pages/users/users.component';
import { UserGuard } from './guards/user.guard';
import { RequestsComponent } from './pages/requests/requests.component';
import { MenusComponent } from './pages/menus/menus.component';
import { EditMenuComponent } from './pages/menus/edit-menu/edit-menu.component';
import { OwnerGuard } from './guards/owner.guard';
import { AdminGuard } from './guards/admin.guard';
import { EditItemComponent } from './pages/menu-items/edit-item/edit-item.component';
import { AdditionsComponent } from './pages/additions/additions.component';
import { LanguagesComponent } from './pages/languages/languages.component';
import { AllLanguagesComponent } from './pages/all-languages/all-languages.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { EditOrderComponent } from './pages/orders/edit-order/edit-order.component';

var childRoutes: Routes = [
  { path: '', redirectTo: 'users', pathMatch: 'full' },
  { path: 'users', component: UsersComponent, },
  { path: 'menus', component: MenusComponent },
  { path: 'menus/:id', component: EditMenuComponent },
  { path: 'menus/items/:id', component: EditItemComponent },
  { path: 'additions', component: AdditionsComponent },
  { path: 'orders', component: OrdersComponent },
  { path: 'orders/:id', component: EditOrderComponent },
  { path: 'menus/additions', component: AdditionsComponent },
  { path: 'languages', component: LanguagesComponent },
  { path: 'allLanguages', component: AllLanguagesComponent },
  { path: 'requests', component: RequestsComponent },
  { path: 'vendors', component: VendorComponent },
  { path: 'vendors/:id', component: EditVendorComponent },
];

const routes: Routes = [
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  { path: 'pages', component: PagesComponent, children: childRoutes, canActivate: [UserGuard] },
  { path: 'signin', component: LoginComponent },
  { path: '**', redirectTo: 'signin' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
