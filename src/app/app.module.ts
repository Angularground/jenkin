import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagesComponent } from './pages/pages.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {
  MatCardModule,
  MatTableModule, MatExpansionModule,
  MatListModule, MatButtonModule, MatIconModule,
  MatInputModule, MatSelectModule, MatFormFieldModule,
  MatToolbarModule, MatMenuModule, MatTooltipModule,
  MatDialogModule, MatDatepickerModule, MatNativeDateModule, MatChipsModule, MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule, MatCheckboxModule, MatDrawer, MatDrawerContent, MatDrawerContainer, MatSidenavModule, MatSliderModule, MatSnackBarModule, MatAutocompleteModule, MatTabsModule,
} from '@angular/material';
import { TopbarComponent } from './elements/topbar/topbar.component';
import { SidebarComponent } from './elements/sidebar/sidebar.component';
import { DialogComponent } from './helperComponents/dialog/dialog.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LoginComponent } from './pages/login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { VendorComponent } from './pages/vendor/vendor.component';
import { UsersComponent } from './pages/users/users.component';
import { MessageComponent } from './helperComponents/message/message.component';
import { EditVendorComponent } from './pages/vendor/edit-vendor/edit-vendor.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { CropperComponent } from './shared/components/image-cropper/cropper/cropper.component';
import { ImgPreviewDirective } from './directives/img-preview.directive';
import { AgmCoreModule } from '@agm/core';
import { SpinnerComponent } from './helperComponents/spinner/spinner.component';
import { RequestsComponent } from './pages/requests/requests.component';
import { MenusComponent } from './pages/menus/menus.component';
import { EditMenuComponent } from './pages/menus/edit-menu/edit-menu.component';
import { MenuItemsComponent } from './pages/menu-items/menu-items.component';
import { EditItemComponent } from './pages/menu-items/edit-item/edit-item.component';
import { AdditionsComponent } from './pages/additions/additions.component';
import { DatePipe } from '@angular/common';
import { LanguagesComponent } from './pages/languages/languages.component';
import { AllLanguagesComponent } from './pages/all-languages/all-languages.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { EditOrderComponent } from './pages/orders/edit-order/edit-order.component';

const MaterialModule = [
  MatSidenavModule,
  MatCardModule,
  MatTableModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatListModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatFormFieldModule,
  MatToolbarModule,
  MatMenuModule,
  MatTooltipModule,
  MatDialogModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatChipsModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatSnackBarModule,
  MatAutocompleteModule,
  MatTabsModule,
  
]

@NgModule({
  declarations: [
    AppComponent,
    PagesComponent,
    TopbarComponent,
    SidebarComponent,
    DialogComponent,
    LoginComponent,
    VendorComponent,
    UsersComponent,
    MessageComponent,
    EditVendorComponent,
    CropperComponent,
    ImgPreviewDirective,
    SpinnerComponent,
    RequestsComponent,
    MenusComponent,
    EditMenuComponent,
    MenuItemsComponent,
    EditItemComponent,
    AdditionsComponent,
    LanguagesComponent,
    AllLanguagesComponent,
    OrdersComponent,
    EditOrderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ImageCropperModule,
    AgmCoreModule.forRoot({
      apiKey:"AIzaSyAVqxJGOu6uyt145Z11llIeMqCxuz48sAA",
      libraries:['places']
    })
  ],
  entryComponents:[CropperComponent],
  providers: [
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
