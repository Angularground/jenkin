import { Component, OnInit } from '@angular/core';
import { DialogService } from 'src/app/helperComponents/dialog/dialog.service';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  tabs = [];

  user:User = new User();

  show:boolean = false;

  constructor(
    private dialogService:DialogService,
    private msgService:MessageService,
    private userService:UserService,
    private router:Router
  ) {

    this.user = JSON.parse(window.localStorage.getItem('user'));

      if(this.user.role == 'owner'){
        this.tabs = [
          { name: 'Dashboard', link: 'dashboard', icon: 'fa-database', hidden: true },
          { name: 'Employees', link: 'users', icon: 'fa-user', hidden: false },
          { name: 'RESTAURANTS', link: 'vendors/'+this.user.restaurantId, icon: 'fa-home', hidden: false },
          { name: 'requests', link: 'requests', icon: 'fa-comments', hidden: true },
          { name: 'menu lists', link: 'menus', icon: 'fa-th', hidden: false },
          { name: 'orders', link: 'orders', icon: 'fa-tags', hidden: false },
          { name: 'languages', link: 'languages', icon: 'fa-language', hidden: false },
        ]
      }

      if(this.user.role == 'admin'){
        this.tabs = [
          { name: 'Dashboard', link: 'dashboard', icon: 'fa-database', hidden: true },
          { name: 'Users', link: 'users', icon: 'fa-user', hidden: false },
          { name: 'RESTAURANTS', link: 'vendors', icon: 'fa-home', hidden: false },
          { name: 'requests', link: 'requests', icon: 'fa-comments', hidden: false },
          { name: 'menu lists', link: 'menus', icon: 'fa-th', hidden: true },
          { name: 'All languages', link: 'allLanguages', icon: 'fa-language', hidden: false },
          { name: 'Feedbacks', link: 'feedbacks', icon: 'fa-comments', hidden: true },
        ]
      }
  }

  ngOnInit() {
  }

  openDialog(temp,style,disableClose?:boolean){
    this.dialogService.openDialog([],temp,style,disableClose);
  }

  closeDialog(temp){
    this.dialogService.close(temp,[]);
  }

  closeAll(){
    this.dialogService.closeAll([]);
  }

  error(value){
    this.msgService.error(value);
  }

  success(value){
    this.msgService.success(value);
  }

  update(){
    this.user.btnDisable = true;
    if(this.user.password){
      this.userService.users.update(this.user.id,{newPassword:this.user.password},null,'updatePassword/'+this.user.id).then(()=>{
        this.user.password = null;
        this.userService.users.update(this.user.id,this.user).then((data) => {
          this.user.btnDisable = false;
          this.closeAll();
          this.success('update detail');
          window.localStorage.setItem('user',data);
          this.user = data;
        }).catch(err => {
          this.user.btnDisable = false;
          this.error(err);
        });
      })
    }else{
      this.userService.users.update(this.user.id,this.user).then((data) => {
        this.user.btnDisable = false;
        this.closeAll();
        this.success('update detail');
        // window.localStorage.setItem('user',data);
        this.user = data;
      }).catch(err => {
        this.user.btnDisable = false;
        this.error(err);
      });
    }
  }

  logOut() {
    window.localStorage.clear();
    this.router.navigate(['/signin']);
  }

}
