import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  @Output() reply:EventEmitter<any> = new EventEmitter();
  show:boolean;

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
  }

  sidebarToggle(){
    this.reply.emit(true);
  }

  logOut() {
    window.localStorage.clear();
    this.router.navigate(['/signin']);
  }

}
