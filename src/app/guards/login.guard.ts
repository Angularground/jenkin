import { Injectable } from '@angular/core';
import { Router, Route } from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

@Injectable({
  providedIn:'root'
})
export class LoginGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

    if (window.localStorage.getItem('token')) {
      this.router.navigate(['/pages']);
      console.log(`LoginGuard Token Found`)
      return false;
    }
    console.log(`LoginGuard not Token Found`)
    return true;
  }
}
