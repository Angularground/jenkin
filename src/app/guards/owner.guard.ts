import { Injectable } from '@angular/core';
import { Router, Route } from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

@Injectable({
  providedIn:'root'
})
export class OwnerGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

    let user = JSON.parse(window.localStorage.getItem('user'));

    if(user.role == 'owner'){
        return true;
    }else{
        return false; 
    }
  }
}
