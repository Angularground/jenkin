import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class DialogService {

  temps = [];

    constructor() {

    }

    openDialog(temps:Array<any>,temp,style,disableClose?:boolean){
        if(temp){
          this.temps.push({
            temp: temp,
            style:style,
            disableClose:disableClose ? disableClose : false
          });
        }
      }

    close(matchTemp, temps: Array<any>) {
        if (this.temps.length && matchTemp) {
            let i = this.temps.findIndex(e => e.temp == matchTemp);
            this.temps.splice(i, 1);
        }
    }

    closeAll(temps:Array<any>){
        temps = [];
        this.temps = [];
    }

}