import { Message } from "../../models/message.model";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn:'root'
})

export class MessageService {

    msgs = [];

    constructor( ){}

    error(msg:string,msgs?:Array<any>,bg?:string,color?:string){
        let error = new Message();
        error.msg  = msg;
        error.sucess = false;
        error.bg = bg;
        error.color = color;
        if(!bg){
            error.bg = 'red';
        }
        if(!color){
            error.color = 'white';
        }
        if(!msgs){
            this.msgs.splice(0, 0, error);
        }else{
            this.msgs.splice(0, 0, error);
        }
        
    }

    success(msg:string,msgs?:Array<any>,bg?:string,color?:string){
        let success = new Message();
        success.msg  = msg;
        success.sucess = true;
        success.bg = bg;
        success.color = color;
        if(!bg){
            success.bg = 'green';
        }
        if(!color){
            success.color = 'white';
        }

        if(!msgs){
            this.msgs.splice(0, 0,success);
        }else{
            this.msgs.splice(0, 0, success);
        }
        
    }
    

}