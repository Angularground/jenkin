import { SizeVariant } from './menu.model';

export class Addition {
    id?: number;
    additionId?:any;
    languages?:Array<any> = [];
    itemId?:any;
    errName?:string;
    btnDisable?:boolean = false;
    type?:string;
    restaurantId?:any;
    additionVariants?:Array<SizeVariant> = [];
}
