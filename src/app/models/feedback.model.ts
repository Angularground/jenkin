import { User } from "./user";

export class Feedback {
    id?: number;
    subject?: number;
    text?: number;
    title?:string;
    type?:string;
    description?:string;
    createdAt?:string;
    user?:User
    constructor() {
     
    }
    reset() {
      this.id = null;
  }
  }
  