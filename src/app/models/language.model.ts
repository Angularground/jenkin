export class Language {
    name?: string;
    errName?:string;
    description?:string;
    type?:string;
    code?: string;
    errCode?:string;
    id?: number;
    languageId?:any;
    selected?: boolean;
    createdAt?: string;
    updatedAt?: string;
    btnDisable?:boolean;
}

export class LanguageOuter {
    id?: number;
    createdAt?: string;
    updatedAt?: string;
    restaurantId?: number;
    languageId?: number;
    language?: Language = new Language();
}