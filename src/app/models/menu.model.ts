import { Language } from './language.model';

export class Menu {
    id?: number;
    menuId?: any;
    imgUrl?: string;
    name?: string;
    restaurantId?: number;
    languages?: Array<Language> = [];
    btnDisable?: boolean;
    errName?: string;
}

export class Item {
    id?: number;
    name?: string;
    itemId: any;
    languages?:Array<any> = [];
    btnDisable?: boolean;
    errName?: string;
    menuId?: number;
    restaurantId?: number;
    imgUrl?: any;
    description?: string;
    errDescription?: string;
    avgRating?: number;
    type?: 'veg' | 'non-veg' = 'veg';
    sizeVariants?: Array<SizeVariant> = [];
    additions?: Array<SizeVariant> = [];
}

export class SizeVariant {
    id?: number;
    sizeVariantId?:any;
    additionVariantId?:any;
    languages?:Array<any> = [];
    name?: string;
    errName?: string;
    price?: number;
    errPrice?: string;
    btnDisable?: boolean;
    itemId?: number;
    additionId?: any;
    restaurantId?: any;
}