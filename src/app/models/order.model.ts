
export class Order {
  id?: number;
  subTotal?: number;
  tax?: number;
  total?: number;
  status?: string;
  paymentStatus?: string;
  orderItems?: Array<OrderItems> = [];
  btnDisable?:boolean;
  constructor() { }
}

export class OrderItems {
  id?: number;
  sizeVariant?: string;
  addOn?:string;
  quantity?: number;
  price?: number
}


