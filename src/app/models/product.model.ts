export class Product {
    id?:number;
    barCode?:string;
    name?: string;
    description?: string;
    shortDescription?: string;
    discount?: number;
    categoryId?: number;
    subCategoryId?: number;
    status?:string;
    offerPrice?:string;
    useOfferPrice?:boolean =false;
    price?:string;
    productNo?:string;
    storeId?:number;
    productAttachments?:Array<CropImages> = [];
    category?:Category;
    subCategory?:Category;
    variants?: Array<Variant> = [];
    images?:Array<CropImages> = [];
    quantityTypeValue?:any;
    quantityType?:any;
    quantityTypeId?:number;
    btn?:boolean;
}

export class Variant {
    id?:number;
    quantityTypeId?: number;
    productId?: number;
    quantityTypeValue?:Quantity;
    price?: number;
    inStock?:boolean;
}

export class Category{
    id?:number;
    name?:string;
    status?:string;
    value?:string;
    imgUrl?: string;
    description:string;
    categoryId?:number;
    add:boolean;
    category?:Category;
}

export class CropImages {
    imgUrl?:string;
    file?:any;
    id?:number;
  }

  export class Quantity{
      id?:number;
      value?:number;
      quantityType:QuantityType;
  }

  export class QuantityType{
      id?:number;
      name?:string;
      add?:boolean = false;
      btn?:boolean = false;
  }

  export class ImportExcel{
    categoryId?:number;
    storeId?:number;
    disableBtn?:boolean;
}