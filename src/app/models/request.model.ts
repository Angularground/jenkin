export class Requests{
    id?:number;
    restaurantName?:string;
    phone?:number;
    email?:string;
    message?:string;
    btnDisable?:boolean;
}