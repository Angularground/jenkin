
export class User {
  id?: number;
  lastName?: string;
  username ?: string;
  errUserName?: string;
  name?: string;
  errName?:string;
  token?: string;
  status?: string;
  email?: string;
  role?: string = "";
  imgUrl?: string;
  password?: string;
  showPassword?:boolean;
  errPassword?:string;
  oldPassword?: string;
  newPassword?: string;
  referralCode?: number;
  location?:string;
  phone?: any;
  createdAt?: string;
  time?: string;
  currentCoordinates?:Array<any> =[];
  deviceId?: string;
  deviceType?: string;
  loginType?:string;
  restaurantId ?:number;
  dob?: string;
  btnDisable?:boolean;
  constructor(user?: User) {
    this.id = user && user.id ? user.id : null;
    this.token = user && user.token ? user.token : '';
    this.status = user && user.status ? user.status : '';
    this.email = user && user.email ? user.email : '';
    this.imgUrl = user && user.imgUrl ? user.imgUrl : '';

  }
  reset() {
    this.id = null;
    this.role = '';
  }
}

export class Download{
	fromDate?:string;
	toDate?:string;
}