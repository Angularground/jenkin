import { LanguageOuter, Language } from './language.model';


export class Vendor {
	name?: string;
	errName?:string;
	title?:string;
	errTitle?:string;
	description?: string;
	errDescription?:string;
	imgUrl?: any;
	availFrom?: string;
	availTo?: string;
	errAvailFrom?: string;
	errAvailTo?: string;
	sunday?: boolean = false;
	monday?:boolean = true;
	tuesday?:boolean = true;
	wednesday?:boolean = true;
	thursday?:boolean = true;
	friday?:boolean = true;
	saturday?:boolean = false;
	location?: string;
	errLocation?: string;
	coordinates?: Array<number> = [];
	phone?: number;
	id?: number;
	status?: string;
	btnDisable?: boolean = false;
	amountPaid?: number;
	pendingAmount?:number;
	availEarning?: number;
	createdAt?: number;
	totalOrderCount?: number;
	pendingOrderCount?: number;
	languageIds?:Array<any> = [];
	languages?:Array<Language> = [];
	restaurantLanguages?:Array<LanguageOuter> = [];
	restaurantAttachments?:Array<any> = [];
	restaurantCategories?:Array<any> = [];
	rating?:any;
	bankDetail?:Bank;
}


export class Bank {
	accountNo?:string;
	errAccountNo?:string;
	bankName?:string;
	errBankName?:string;
	createdAt?:string;
	attachments?:Array<any> = [];
	holderName?:string;
	errHolderName?:string;
	id?: number;
	ifscCode?:string;
	errIfscCode?:string;
	updatedAt?:string;
	restaurantId ?:number;
	btnDisable?:boolean;
  }
