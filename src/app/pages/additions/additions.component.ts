import { Component, OnInit } from '@angular/core';
import { Page } from 'src/app/shared/common/contracts/page';
import { SizeVariant } from 'src/app/models/menu.model';
import { AdditionService } from 'src/app/service/addition.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { Addition } from 'src/app/models/addition.model';

@Component({
  selector: 'app-additions',
  templateUrl: './additions.component.html',
  styleUrls: ['./additions.component.css']
})
export class AdditionsComponent implements OnInit {

  additions:Page<Addition>;
  subscription:Subscription;
  loading:boolean = true;
  temp = 'list';
  addition = new Addition();
  varient = new SizeVariant();
  itemId:any;

  constructor(
    private additionService:AdditionService,
    private activatedRoute:ActivatedRoute,
    private msgService: MessageService,
  ) {
    this.additions = new Page({
      api:additionService.additions,
      properties:new Addition(),
      filters:[{
        field:'itemId',
        value:null
      },{
        field:'name',
        value:null
      }]
    });

    this.subscription = this.activatedRoute.queryParams.subscribe(params => {
      this.additions.filters.properties['itemId'].value = params.itemId;
      this.itemId = params.itemId;
    });

    this.fetch(true);

   }
  
   error(value) {
    this.msgService.error(value);
  }

  success(value) {
    this.msgService.success(value);
  }

  ngOnInit() {
  }

  fetch(loading?:boolean){
    this.loading = loading
    this.additions.fetch().then(() => {
      this.loading = false;
      this.additions.items.forEach(i => {
        i.btnDisable = false;
      })
    }).catch(err => {
      this.error(err);
    });
  }

  edit(addition?:Addition){
    if(addition){
      this.addition = addition;
    }else{
      this.addition = new Addition();
    }
    this.varient = new SizeVariant();
    this.temp = 'create';
  }

  delete(addition:Addition){
    this.additionService.additions.remove(addition.id).then(() => {
      this.success('delete');
      this.fetch(false);
    }).catch(err => {
      this.error(err);
    });
  }

  create(){
    if(!this.addition.type){
     return this.addition.errName = "Name is require";
    }
    this.addition.itemId = this.itemId;
    this.addition.btnDisable = true;
    this.addition.restaurantId = window.localStorage.getItem('restaurantId');
    this.additionService.additions.create(this.addition).then(() => {
      this.addition.btnDisable = false;
      this.success('add Addition');
      this.addition = new Addition();
      this.fetch(true);
      this.temp = 'list'
    }).catch((err) => {
      this.addition.btnDisable = false;
      this.error(err);
    });
  }

  update(){
    if(!this.addition.type){
      return this.addition.errName = "Name is require";
     }
     this.addition.btnDisable = true;
     this.additionService.additions.update(this.addition.id,this.addition).then(() => {
       this.addition.btnDisable = false;
       this.success('update');
       this.addition = new Addition();
       this.fetch(true);
       this.temp = 'list'
     }).catch((err) => {
       this.addition.btnDisable = false;
       this.error(err);
     })
  }

  addVarient(){
    if(!this.varient.name){
      return this.varient.errName = "Name is require";
    }
    if(!this.varient.price){
      return this.varient.errPrice = "Price is require";
    }
    this.varient.btnDisable = true;
    if(this.addition.id){
      this.varient.itemId = this.itemId;
      this.varient.restaurantId = window.localStorage.getItem('restaurantId');
      this.varient.additionId = this.addition.id;
      this.additionService.varients.create(this.varient).then((data) => {
        this.varient = new SizeVariant();
        this.success('add varient');
        this.addition.additionVariants.push(data);
        this.varient.btnDisable = false;
      }).catch(err =>{
        this.varient.btnDisable = false;
        this.error(err);
        })
    }else{
      this.addition.additionVariants.push(this.varient);
      this.success('add varient');
      this.varient = new SizeVariant();
    }
  }

  deleteVarient(varient:SizeVariant,i){
    if(this.addition.id){
      this.additionService.varients.remove(varient.id).then(() =>{
        this.success('delete');
        this.addition.additionVariants.splice(i,1);
      }).catch(err => {
        this.error(err);
      });
    }else{
      this.addition.additionVariants.splice(i,1);
    }
  }
  

}
