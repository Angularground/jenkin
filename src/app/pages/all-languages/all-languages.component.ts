import { Component, OnInit } from '@angular/core';
import { Page } from 'src/app/shared/common/contracts/page';
import { Language } from 'src/app/models/language.model';
import { LanguageService } from 'src/app/service/language.servcie';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { DialogService } from 'src/app/helperComponents/dialog/dialog.service';

@Component({
  selector: 'app-all-languages',
  templateUrl: './all-languages.component.html',
  styleUrls: ['./all-languages.component.css']
})
export class AllLanguagesComponent implements OnInit {

  languages:Page<Language>;
  loading:boolean = true;

  language:Language = new Language();

  constructor(
    private languageService:LanguageService,
    private msgService:MessageService,
    private dialogService:DialogService
  ) {
    this.languages = new Page({
      api:languageService.languages,
      properties:new Language,
      serverPaging:false,
      filters:[{
        field:'name',
        value:null,
      }]
    });
    this.fetch(true);
   }

  ngOnInit() {
  }

  error(value){
    this.msgService.error(value);
  }

  success(value){
    this.msgService.success(value);
  }

  openDialog(temp,style,disableClose){
    this.dialogService.openDialog([],temp,style,disableClose);
  }

  closeDialog(temp){
    this.dialogService.close(temp,[]);
  }

  closeAll(){
    this.dialogService.closeAll([]);
  }

  fetch(loading?:boolean){
    this.loading = loading;
    this.languages.fetch().then(() => {
      this.loading = false;
    }).catch(err =>{
      this.loading = false;
      this.error(err);
    });
  }

  clearForm(){
    this.language.name = null;
    this.language.code = null;
  }

  beForeUpdate(language:Language,temp){
    this.language = language;
    this.openDialog(temp,{'width':'320px'},false);
  }

  update(){
    this.language.btnDisable = true;
    this.languageService.languages.update(this.language.id,this.language).then(() => {
      this.language.btnDisable = false;
      this.success('update language');
      this.fetch(false);
      this.closeAll();
    }).catch((err) => {
      this.language.btnDisable = false;
      this.error(err);
      this.fetch(false);
    })
  }

  beForeCreate(temp){
    this.language = new Language();
    this.openDialog(temp,{'width':'320px'},false);
  }

  addNew(){
    if(!this.language.name){
      return this.language.errName = "Name is require"
    }
    if(!this.language.code){
      return this.language.errCode = "Name is code"
    }
    this.language.btnDisable = true;
    this.languageService.languages.create(this.language).then(() => {
      this.language.btnDisable = false;
      this.success('add Language');
      this.closeAll();
      this.fetch(false);
    }).catch((err) => {
      this.language.btnDisable = false;
      this.error(err);
    })
  }

  delete(id){
    this.language.btnDisable = true;
    this.languageService.languages.remove(id).then(() => {
      this.language.btnDisable = false;
      this.success('delete');
      this.fetch(false);
    }).catch(err => {
      this.error(err);
      this.language.btnDisable = false;
    })
  }

}
