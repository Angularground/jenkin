import { Component, OnInit } from '@angular/core';
import { Page } from 'src/app/shared/common/contracts/page';
import { Language } from 'src/app/models/language.model';
import { LanguageService } from 'src/app/service/language.servcie';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { VendorService } from 'src/app/service/vendor.service';
import { MenuService } from 'src/app/service/menu.service';
import { Menu, Item, SizeVariant } from 'src/app/models/menu.model';
import { Addition } from 'src/app/models/addition.model';
import { AdditionService } from 'src/app/service/addition.service';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.css']
})
export class LanguagesComponent implements OnInit {

  languages:Array<Language> = [];
  restaurantId = window.localStorage.getItem('restaurantId');
  loading:boolean = true;
  entity:Array<any> = [];
  menus:Page<Menu>;
  items:Page<Item>;
  sizeVarients:Page<SizeVariant>;
  additions:Page<Addition>;
  additionsVarients:Page<SizeVariant>;
  LanguageType:any;
  entityId:any;
  itemType:any;

  filter = [{ field:'restaurantId', value:this.restaurantId }]

  constructor(
    private languageSevice:LanguageService,
    private msgService:MessageService,
    private vendorService:VendorService,
    private menuService:MenuService,
    private additionService:AdditionService
  ) {
    this.fetch(true);

    this.menus = new Page({
      api:menuService.languageGet,
      serverPaging:false,
      properties: new Menu(),
      filters:this.filter
    });
    this.items = new Page({
      api:menuService.itemsForLanguage,
      serverPaging:false,
      properties: new Item(),
      filters:this.filter
    });
    this.sizeVarients = new Page({
      api:menuService.sizeVariantsForLanguage,
      serverPaging:false,
      properties: new SizeVariant(),
      filters:this.filter
    });
    this.additions = new Page({
      api:additionService.additionsForLanguage,
      serverPaging:false,
      properties: new Addition(),
      filters:this.filter
    });
    this.additionsVarients = new Page({
      api:additionService.additionVarientsForLanguage,
      serverPaging:false,
      properties: new SizeVariant(),
      filters:this.filter
    });
   }

  ngOnInit() {
  }

  error(value){
    this.msgService.error(value);
  }

  success(value){
    this.msgService.success(value);
  }

  fetch(loading?:boolean){
    this.loading = loading;
    this.vendorService.restaurants.simpleGet({path:'/'+this.restaurantId}).then(data => {
      this.loading = false;
      this.languages = [];
        data.restaurantLanguages.forEach(i => {
          this.languages.push(i.language);
        });
        if(!this.entityId){
          this.fetchMenu();
        }
    }).catch(err => {
      this.loading = false;
      this.error(err);
    });
  }

  fetchMenu(){
    this.itemType = 'name';
    this.LanguageType = 'menuLanguages';
    this.entityId = 'menuId';
    this.loading = true;
    this.menus.fetch().then(data => {
      this.loading = false;
      this.setDataOnEntity(data);
    }).catch(err => this.error(err));
  }

  fetchItem(type){
    this.itemType = type;
    this.LanguageType = 'itemLanguages';
    this.entityId = 'itemId';
    this.loading = true;
    this.items.fetch().then(data => {
      this.loading = false;
      this.setDataOnEntity(data);
    }).catch(err => this.error(err));
  }

  fetchSizeVarient(){
    this.itemType = 'name';
    this.LanguageType = 'sizeVariantLanguages';
    this.entityId = 'additionId';
    this.loading = true;
    this.sizeVarients.fetch().then(data => {
      this.loading = false;
      this.setDataOnEntity(data);
    }).catch(err => this.error(err));
  }

  fetchAddition(){
    this.itemType = 'name';
    this.LanguageType = 'additionLanguages';
    this.entityId = 'additionId';
    this.loading = true;
    this.additions.fetch().then(data => {
      this.loading = false;
      this.setDataOnEntity(data);
    }).catch(err => this.error(err));
  }

  fetchAdditionVarient(){
    this.itemType = 'name';
    this.LanguageType = 'additionVariantLanguages';
    this.entityId = 'additionVariantLists';
    this.loading = true;
    this.additionsVarients.fetch().then(data => {
      this.loading = false;
      this.setDataOnEntity(data);
    }).catch(err => this.error(err));
  }

  CreateAndUpdate(entity,value){
    if(value.btnDisable == true){
      return;
    }
      value[this.entityId] = entity[this.entityId];
    this.languageSevice[this.LanguageType].create(value,'one/entry').then((data) => {
      value.btnDisable = true;
    }).catch(err => {
      this.error(err);
    })
  }

  setDataOnEntity(data){
    data.items.forEach(i => {

      if(i.languages || i.languages.length){
        let languageIndex = 0;
        let language = [];
        i.languages.forEach((e => {
          if(i.languages[languageIndex] && languageIndex >= this.languages.length){
            let index = i.languages.findIndex((l) => l == e);
            i.languages.splice(index,1);
          }
          if(i.languages[languageIndex] && languageIndex <= this.languages.length){
            let indexOFLanguage =  this.languages.findIndex((l) => l.id == e.languageId);
            if(indexOFLanguage > -1){
              let l = this.languages[indexOFLanguage];
            this.languages.splice(indexOFLanguage,1);
            this.languages.splice(languageIndex,0,l);
            language.push(e);
            }
            languageIndex = languageIndex + 1;
          }
        }));

        if(language.length){
          i.languages = language;
        }

        i.languages.forEach((e) => {
          e.btnDisable = true;
        })

      }

      if(!i.languages || i.languages.length != this.languages.length ){
        let index = 0;
        if(!i.languages.length){
          i.languages = [];
        }

        for(let l of this.languages){
          if(!i.languages[index]){
            let language = new Language();
           language.languageId = l.id;
           language.name = null;
           language.type = null;
           language.description = null;
           language.btnDisable = true;
          i.languages.push(language);
          }
          index = index + 1;
        }
      }
    });
    this.entity = data.items;
  }

}
