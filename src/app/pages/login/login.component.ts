import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;
  isloading:boolean = false;
  errorTxt:string;

  constructor(
    public router:Router,
    public formbuilder:FormBuilder,
    public userService:UserService,
  ) {
    this.loginForm = this.formbuilder.group({
      password: ['', [Validators.required]],
      email: ['', [Validators.required]],
    });
   }

  ngOnInit() {
  }

  login(){

    if(!this.loginForm.value.email){
      return this.errorTxt = 'Fill Email';
    }

    if(!this.loginForm.value.password){
      return this.errorTxt = 'Fill password';
    }

    this.isloading = true;
    this.userService.signIn.create(this.loginForm.value).then((data) => {
      if(!data.role || data.role == "normal"){
        this.isloading = false;
        return  this.errorTxt = 'Only Admin and vendor Accessible !';
      }
      window.localStorage.setItem('token',data.token);
      window.localStorage.setItem('user',JSON.stringify(data));
      this.isloading = false;
      if (data.role == 'admin') {
        this.router.navigate(['/pages']);
      }
  
      if (data.role == 'owner') {
        this.router.navigate(['/pages']);
        if(data.restaurantId){
          window.localStorage.setItem('restaurantId',data.restaurantId);
        }
      }
    }).catch(err => {
      this.isloading = false;
      this.errorTxt = 'Invalid username and password';
    });
  }

}
