import { Component, OnInit } from '@angular/core';
import { Item, SizeVariant } from 'src/app/models/menu.model';
import { Model } from 'src/app/shared/common/contracts/model';
import { MenuService } from 'src/app/service/menu.service';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { CropperService } from 'src/app/shared/components/image-cropper/cropper.service';
import { DialogService } from 'src/app/helperComponents/dialog/dialog.service';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {

  item: Model<Item>;
  sizeVariant = new SizeVariant();
  msgs = [];
  loading: boolean = true;
  subscription: Subscription;
  id: any;
  temps = [];
  selectImg: any = null;
  user = JSON.parse(window.localStorage.getItem('user'));
  menuId:any;

  constructor(
    private itemService: MenuService,
    private mesgServcie: MessageService,
    private activatedRoute: ActivatedRoute,
    public cropperService: CropperService,
    public dialogService: DialogService,
    private router:Router
  ) {
    this.item = new Model({
      api: itemService.items,
      properties: new Item()
    });

    this.subscription = this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    });

    this.subscription = this.activatedRoute.queryParams.subscribe(params => {
      this.menuId = params.menuId;
    });

    this.fetch(true);

  }

  ngOnInit() {
  }

  error(value) {
    this.mesgServcie.error(value, this.msgs);
  }

  success(value) {
    this.mesgServcie.success(value, this.msgs);
  }

  openDialog(temp, style, disableClose?: boolean) {
    this.dialogService.openDialog(this.temps, temp, style, disableClose);
  }

  fetch(loading?: boolean) {
    this.loading = loading;
    if (this.id != 'new') {
      this.item.fetch(this.id).then(() => {
        this.loading = false;
      }).catch(err => {
        this.loading = false;
        this.error(err);
      });
    } else {
      this.loading = false;
      this.item.properties = new Item();
    }
  }

  onSelectImg(event, temp, style) {
    if (event.target.files.length) {
      this.selectImg = event;
      this.openDialog(temp, style);
    }
  }

  closeDialog(temp) {
    this.dialogService.close(temp, this.temps);
  }

  getCropImg(res, temp) {
    this.closeDialog(temp);
    if (res.error) {
      return this.error(res.error);
    }
    if (this.selectImg.target.value) {
      this.selectImg.target.value = null;
    }
    return this.item.properties.imgUrl = res;
  }

  uploadImg(cropImg, progress?: any, callBack?: any) {
    let file = cropImg.file;
    this.cropperService.setUploader();
    this.cropperService.setUploderOptions({ url: '/api/images/upload' });
    this.cropperService.addToQueue(file);
    if (progress && this.cropperService.uploader.queue.length) {
      this.openDialog(progress, { 'background': 'transparent' }, true);
    }
    this.cropperService.uploadAll((data, err, complate) => {
      if (err) {
        this.dialogService.closeAll([]);
        return this.error(err);
      }
      if (complate) {
        this.dialogService.closeAll([]);
      }
      if (data.url) {
        this.dialogService.closeAll([]);
        return callBack(data.url);
      }
    });
  }

  beforeAddItem(temp) {
    if (!this.item.properties.name) {
      return this.item.properties.errName = 'Name is require';
    }
    if (!this.item.properties.description) {
      return this.item.properties.errDescription = 'Description is require';
    }
    if (!this.item.properties.imgUrl) {
      return this.error('image is require');
    }
    if (this.item.properties.imgUrl && this.item.properties.imgUrl.file) {
      this.uploadImg(this.item.properties.imgUrl, temp, (url) => {
        this.item.properties.imgUrl = url;
        this.addItems();
      });
    } else {
      this.addItems();
    }
  }

  clearForm() {
    this.item.properties = new Item();
    this.sizeVariant = new SizeVariant();
  }

  addItems() {
    this.item.properties.menuId = this.menuId;
    this.item.properties.restaurantId = window.localStorage.getItem('restaurantId') as any;
    this.itemService.items.create(this.item.properties).then((data) => {
      this.success('create Item');
      this.id = data.id;
      this.fetch(true);
    }).catch((err) => {
      this.error(err);
    })
  }

  addSizeVariant() {
    if (!this.sizeVariant.name) {
      return this.sizeVariant.errName = 'Size is require';
    }
    if (!this.sizeVariant.price) {
      return this.sizeVariant.errPrice = 'Price is require';
    }
    this.sizeVariant.btnDisable = true;
    if (this.id != 'new') {
      this.sizeVariant.itemId = this.id;
      this.sizeVariant.restaurantId = window.localStorage.getItem('restaurantId');
      this.itemService.sizeVariants.create(this.sizeVariant).then(() => {
        this.success('add size Variant');
        this.sizeVariant = new SizeVariant();
        this.sizeVariant.btnDisable = false;
        this.fetch(false);
      }).catch(err => {
        this.error(err);
        this.sizeVariant.btnDisable = false;
      })
    } else {
      this.item.properties.sizeVariants.push(this.sizeVariant);
      this.sizeVariant = new SizeVariant();
      this.sizeVariant.btnDisable = false;
    }

  }

  beforeUpdate(progress){
    if(this.item.properties.imgUrl.file){
      this.uploadImg(this.item.properties.imgUrl,progress,(url) =>{
        this.item.properties.imgUrl = url;
        this.update();
      });
    }else{
      this.update();
    }
  }

  update(){
    this.item.properties.btnDisable = true;
    this.itemService.items.update(this.item.properties.id,this.item.properties).then(() => {
      this.fetch(false);
      this.item.properties.btnDisable = false;
      this.success('update');
      this.router.navigate(['/pages/menus/'+this.item.properties.menuId]);
    }).catch((err) => {
      this.error(err);
      this.item.properties.btnDisable = false;
    })
  }

  deleteSizeVarient(varient: SizeVariant,i) {
    varient.btnDisable = true;
    if (varient.id) {
      this.itemService.sizeVariants.remove(varient.id).then(() => {
        this.success('delete');
        this.fetch(false);
      }).catch((err) => {
        this.error(err);
      });
    }else{
      this.item.properties.sizeVariants.splice(i,1);
      this.success('delete');
    }
  }

}
