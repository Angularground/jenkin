import { Component, OnInit, Input } from '@angular/core';
import { Page } from 'src/app/shared/common/contracts/page';
import { Item } from 'src/app/models/menu.model';
import { MenuService } from 'src/app/service/menu.service';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: ['./menu-items.component.css']
})
export class MenuItemsComponent implements OnInit {

  @Input() menuId: any;

  menuItems: Page<Item>;
  msg = [];
  loading: boolean = false;

  constructor(
    private menuService: MenuService,
    private msgService: MessageService,
    private router: Router
  ) {
    this.menuItems = new Page({
      api: menuService.items,
      properties: new Item(),
      filters: [{
        field: 'menuId',
        value: null
      }, {
        field: 'name',
        value: null
      }]
    });
  }

  ngOnInit() {
    if (this.menuId) {
      this.menuItems.filters.properties['menuId'].value = this.menuId;
      this.fetch(true);
    }
  }

  error(value) {
    this.msgService.error(value, this.msg);
  }

  success(value) {
    this.msgService.success(value, this.msg);
  }

  fetch(loading?: boolean) {
    this.loading = loading;
    this.menuItems.fetch().then(() => {
      this.loading = false;
      this.menuItems.items.forEach(i => {
        i.avgRating = parseInt(i.avgRating as any);
      })
    }).catch(err => {
      this.error(err);
    });
  }

  newItem() {
    this.router.navigate(['/pages/menus/items/new'], { queryParams: { menuId: this.menuId } })
  }

  viewAdditions(id) {
    this.router.navigate(['/pages/additions'], { queryParams: { itemId: id } })
  }

  delete(item: Item) {
    item.btnDisable = true;
    this.menuService.items.remove(item.id).then(() => {
      item.btnDisable = false;
      this.fetch(false);
      this.success('delete');
    }).catch(err => {
      item.btnDisable = false;
      this.fetch(false);
      this.error(err);
    });
  }

}
