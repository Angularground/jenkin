import { Component, OnInit } from '@angular/core';
import { Model } from 'src/app/shared/common/contracts/model';
import { Menu, Item } from 'src/app/models/menu.model';
import { MenuService } from 'src/app/service/menu.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { DialogService } from 'src/app/helperComponents/dialog/dialog.service';
import { CropperService } from 'src/app/shared/components/image-cropper/cropper.service';

@Component({
  selector: 'app-edit-menu',
  templateUrl: './edit-menu.component.html',
  styleUrls: ['./edit-menu.component.css']
})
export class EditMenuComponent implements OnInit {

  msg = [];
  menu:Model<Menu>;
  temps = [];
  temp:any = 'create';
  subscription:Subscription;
  id:any;
  loading:boolean = true;
  selectImg:any = null;
  user = JSON.parse(window.localStorage.getItem('user'));
  item = new Item();

  constructor(
    private menuService:MenuService,
    private activatedRoute:ActivatedRoute,
    private msgService:MessageService,
    public dialogService:DialogService,
    public cropperService:CropperService
  ) {
    this.menu = new Model({
      api:menuService.menus,
      properties:new Menu()
    });

    this.subscription = this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    });

   }

  ngOnInit() {
    this.fetch(true);
  }

  error(value){
    this.msgService.error(value,this.msg);
  }

  success(value){
    this.msgService.success(value,this.msg);
  }

  openDialog(temp,style,disableClose?:boolean){
    this.dialogService.openDialog(this.temps,temp,style,disableClose);
  }

  closeDialog(temp){
    this.dialogService.close(temp,this.temps);
  }

  fetch(loading?:boolean){
    this.loading = loading;
    if(this.id != 'new'){
      this.temp = 'detail';
      this.menu.fetch(this.id).then(() =>{
        this.loading = false;
        this.menu.properties.btnDisable = false;
      }).catch(err => {
        this.error(err);
        this.loading = false;
      });
    }else{
      this.menu.properties = new Menu();
      this.menu.properties.btnDisable = false;
      this.loading = false;
    }
  }

  onSelectImg(event, temp, style) {
    if (event.target.files.length) {
      this.selectImg = event;
      this.openDialog(temp, style);
    }
  }

  getCropImg(res, temp) {
    this.closeDialog(temp);
    if (res.error) {
      return this.error(res.error);
    }
      return this.menu.properties.imgUrl = res;
  }

  uploadImg(cropImg,progress?:any,callBack?:any){
    let file = cropImg.file;
    this.cropperService.setUploader();
    this.cropperService.setUploderOptions({url:'/api/images/upload'});
    this.cropperService.addToQueue(file);
    if(progress && this.cropperService.uploader.queue.length){
      this.dialogService.openDialog(this.temps,progress,{'background':'transparent'},true);
    }
    this.cropperService.uploadAll((data,err,complate) => {
      if(err){
        this.dialogService.closeAll([]);
       return this.error(err);
      }
      if(complate){
        this.dialogService.closeAll([]);
      }
      if(data.url && callBack){
        return callBack(data.url);
      }
      if(data.url && !callBack){
        this.dialogService.closeAll([]);
        this.menu.properties.imgUrl = data.url;
        if(this.id == 'new'){
          this.create();
        }else{
          this.update();
        }
      }
    });
  }

  beforeCreate(temp){
    if(!this.menu.properties.name){
      return this.menu.properties.errName = 'Name is require';
    }
    if(!this.menu.properties.imgUrl){
      return this.error('cover image require');
    }
    this.uploadImg(this.menu.properties.imgUrl,temp);
  }

  create(){
    this.menu.properties.btnDisable = true;
    this.menu.properties.restaurantId = this.user.restaurantId;
    this.menu.properties.restaurantId = window.localStorage.getItem('restaurantId') as any;
    this.menuService.menus.create(this.menu.properties).then((menu) =>{
      this.success('create');
      this.id = menu.id;
      this.temp = 'detail';
      this.menu.properties.btnDisable = false;
    }).catch(err => {
      this.error(err);
      this.menu.properties.btnDisable = false;
    })
  }

  update(){
    this.menu.properties.btnDisable = true;
    this.menuService.menus.update(this.menu.properties.id,this.menu.properties).then(() =>{
      this.menu.properties.btnDisable = false;
      this.fetch(false);
      this.temp = 'detail';
      this.success('Update');
    }).catch(err => {
      this.menu.properties.btnDisable = false;
      this.fetch(false);
      this.error(err);
    })
  }
}
