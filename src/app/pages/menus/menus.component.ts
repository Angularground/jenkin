import { Component, OnInit, Input } from '@angular/core';
import { Page } from 'src/app/shared/common/contracts/page';
import { Menu } from 'src/app/models/menu.model';
import { MenuService } from 'src/app/service/menu.service';
import { MessageService } from 'src/app/helperComponents/message/message.service';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css']
})
export class MenusComponent implements OnInit {

  @Input() restaurantId:any;

  menus:Page<Menu>;
  msgs = [];
  loading:boolean = true;
  user = JSON.parse(window.localStorage.getItem('user'));

  constructor(
    private menuService:MenuService,
    private msgService:MessageService,
  ) {
    this.menus = new Page({
      api:menuService.menus,
      properties:new Menu(),
      filters:[{
        field:'restaurantId',
        value:null
      },{
        field:'name',
        value:null
      }]
    })
   }

  ngOnInit() {
    if(this.restaurantId){
      this.menus.filters.properties['restaurantId'].value = this.restaurantId;
    }
    if(this.user.restaurantId){
      this.menus.filters.properties['restaurantId'].value = this.user.restaurantId;
    }
    this.fetch(true);
  }

  fetch(loading?:boolean){
    this.loading = loading;
    this.menus.fetch().then(() => {
      this.loading = false;
      this.menus.items.forEach(i =>{
        i.btnDisable = false;
      })
    }).catch((err) => {
      this.error(err);
      this.loading = false;
    });
  }

  error(error){
    this.msgService.error(error,this.msgs);
  }

  success(msg){
    this.msgService.success(msg,this.msgs);
  }

  delete(menu:Menu){
    menu.btnDisable = true;
    this.menuService.menus.remove(menu.id).then(()=>{
      menu.btnDisable = false;
      this.success('delete');
      this.fetch(false);
    }).catch((err) => {
      menu.btnDisable = false;
      this.fetch(false);
      this.error(err);
    })
  }

}
