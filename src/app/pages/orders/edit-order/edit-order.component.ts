import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/models/order.model';
import { Model } from 'src/app/shared/common/contracts/model';
import { OrderService } from 'src/app/service/orders.service';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent implements OnInit {

  order:Model<Order>;
  subscription:Subscription;
  status = ['created','preparing','served','completed','deleted'];
  loading:boolean = true;
  id:any;

  constructor(
    private orderService:OrderService,
    private msgService:MessageService,
    private activatedRoute:ActivatedRoute
  ) {
    this.order = new Model({
      api:orderService.ordersById,
      properties:new Order()
    });

    this.subscription = this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    });

    this.fetch(true);

   }

   error(value){
    this.msgService.error(value);
  }

  success(value){
    this.msgService.success(value);
  }

 ngOnInit() {
 }

 fetch(loading?:boolean){
   this.loading = loading;
   if(this.id != 'new'){
    this.order.fetch(this.id).then(() => {
      this.loading = false;
    }).catch((err) => {
      this.error(err);
      this.loading = false;
    });
   }else{
     this.order.properties = new Order();
   }
 }

 update(msg?:string){
  this.orderService.ordersById.update(this.order.properties.id,this.order.properties).then(() => {
    this.success(msg);
    this.fetch(false);
  }).catch(err => {
    this.fetch(false);
  })
 }

}
