import { Component, OnInit, Input } from '@angular/core';
import { Page } from 'src/app/shared/common/contracts/page';
import { Order } from 'src/app/models/order.model';
import { OrderService } from 'src/app/service/orders.service';
import { MessageService } from 'src/app/helperComponents/message/message.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  @Input() restaurantId:any;

  orders:Page<Order>;
  loading:boolean = true;

  status = ['created','preparing','served','completed','deleted'];
  paymentStatus = ['pending','paid'];

  constructor(
    private orderService:OrderService,
    private msgService:MessageService
  ) {

    this.orders = new Page({
      api:orderService.orders,
      properties:new Order(),
      serverPaging:false,
      filters:[{
        field:'status',
        value:null
      },{
        field:'paymentStatus',
        value:null
      },{
        field:'restaurantId',
        value:null
      },]
    });

   }

   error(value){
     this.msgService.error(value);
   }

   success(value){
     this.msgService.success(value);
   }

  ngOnInit() {
    if(!this.restaurantId){
      this.orders.filters.properties['restaurantId'].value = window.localStorage.getItem('restaurantId');
    }
    if(this.restaurantId){
      this.orders.filters.properties['restaurantId'].value = this.restaurantId;
    }
    this.fetch(true);
  }

  fetch(loading?:boolean){
    this.loading = loading;
    this.orders.fetch().then(() => {
      this.loading = false;
    }).catch((err) => {
      this.error(err);
      this.loading = false;
    });
  }

  delete(order:Order){
    this.orderService.ordersById.remove(order.id).then(() => {
      this.success('delete order');
      this.fetch(false);
    }).catch(err => {
      this.error(err);
    });
  }

  update(order:Order,msg?:string){
    this.orderService.ordersById.update(order.id,order).then(() => {
      this.fetch(false);
      this.success(msg ? msg : 'update');
    }).catch(err => {
      this.fetch(false);
      this.error(err);
    })
  }

}
