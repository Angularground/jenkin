import { Component, OnInit } from '@angular/core';
import { DialogService } from '../helperComponents/dialog/dialog.service';
import { MessageService } from '../helperComponents/message/message.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

  constructor(
    public dialogService:DialogService,
    public messageService:MessageService
  ) { }

  ngOnInit() {
  }

}
