import { Component, OnInit } from '@angular/core';
import { Page } from 'src/app/shared/common/contracts/page';
import { User } from 'src/app/models/user';
import { RequestService } from 'src/app/service/request.service';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { Requests } from 'src/app/models/request.model';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {

  requests:Page<Requests>;
  loading:boolean = true;

  constructor(
    private requestService:RequestService,
    private msgService:MessageService
  ) {
    this.requests = new Page({
      api:requestService.requests,
      properties: new Requests(),
      filters:[{
        field:'name',
        value:null,
      },{
        field:'status',
        value:'active',
      }]
    });
   this.fetch(true);
   }


  ngOnInit() {
  }

  fetch(loading:boolean){
    this.requests.fetch(() => {
      this.loading = false;
      this.requests.items.forEach(element => {
        element.btnDisable = false;
      });
    }).catch(err => {
      this.loading = false;
      this.error(err);
    })
  }

  error(value) {
    this.msgService.error(value);
  }

  success(value) {
    this.msgService.success(value);
  }

  delete(request:Requests){
    request.btnDisable = true;
    this.requestService.requests.remove(request.id).then(()=>{
      request.btnDisable = false;
      this.success('delete');
      this.fetch(false);
    }).catch((err) => {
      request.btnDisable = false;
      this.fetch(false);
      this.error(err);
    })
  }

}
