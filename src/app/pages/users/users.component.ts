import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { Page } from 'src/app/shared/common/contracts/page';
import { User } from 'src/app/models/user';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { DialogService } from 'src/app/helperComponents/dialog/dialog.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  @Input() restaurantId:any;
  @Output() add:EventEmitter<any> = new EventEmitter;

  users:Page<User>;
  loading:boolean = true;
  user = JSON.parse(window.localStorage.getItem('user'));
  temps = [];
  newUser:User = new User();
  role = ['owner','manager', 'employee'];
  status = ['active','inactive','blocked','deleted'];

  constructor(
  private  userService:UserService,
  private msgService:MessageService,
  public dialogService: DialogService,
  ) {
    this.users = new Page({
      api:userService.users,
      properties:new User(),
      filters:[{
        field:'name',
        value:null,
      },{
        field:'status',
        value:'active',
      },{
        field:'restaurantId',
        value:null,
      },]
    })
   }

  ngOnInit() {
    if(this.user.restaurantId){
      this.restaurantId = this.user.restaurantId;
    }
    if(this.restaurantId){
      this.users.filters.properties['restaurantId'].value = this.restaurantId;
    }
    this.fetch(true);

  }

  fetch(loading:boolean){
    this.newUser.role = 'manager';
    this.loading = loading;
    this.users.fetch().then(() =>{
      this.loading = false;
    }).catch(err => {
      this.loading = false;
      this.error(err);
    });
  }

  error(value) {
    this.msgService.error(value);
  }

  success(value) {
    this.msgService.success(value);
  }

  openDialog(temp,style,disableClose?:boolean){
    this.dialogService.openDialog(this.temps,temp,style,disableClose);
  }

  closeDialog(temp){
    this.dialogService.close(temp,this.temps);
  }


  updateStatus(user:User,value){
    user.status = value;
    this.update(user,value);
  }

  update(user:User,msg){
    user.btnDisable = true;
    if(user.password){
      this.userService.users.update(user.id,{newPassword:user.password},null,'updatePassword/'+user.id).then(()=>{
        user.password = null;
        this.userService.users.update(user.id,user).then(() =>{
          this.dialogService.closeAll([]);
          user.btnDisable = false;
          this.newUser = new User();
          this.fetch(false);
          this.success(msg);
        }).catch(err => {
          user.btnDisable = false;
          this.fetch(false);
          this.error(err);
        });
      })
    }else{
      this.userService.users.update(user.id,user).then(() =>{
        this.dialogService.closeAll([]);
        user.btnDisable = false;
        this.newUser = new User();
        this.fetch(false);
        this.success(msg);
      }).catch(err => {
        user.btnDisable = false;
        this.fetch(false);
        this.error(err);
      });
    }
  }

  delete(user:User){
    user.btnDisable = true;
    this.userService.users.remove(user.id).then(()=>{
      user.btnDisable = false;
      this.user = new User();
      this.newUser.role = 'manager';
      this.success('delete');
      this.fetch(false);
    }).catch((err) => {
      user.btnDisable = false;
      this.fetch(false);
      this.error(err);
    })
  }


  createAdmin(temp) {
    let user = this.newUser;
    if (!user.name) {
      return user.errName = 'Name is require';
    }

    if (!user.username) {
      return user.errUserName = 'User Name is require';
    }

    if (!user.password) {
      return user.errPassword = 'Password is require';
    }
    user.restaurantId = this.user.restaurantId;
    user.btnDisable = true;
    this.userService.users.create(user).then(() => {
      this.success('create');
      this.fetch(false);
      user.btnDisable = false;
      this.user = new User();
      this.newUser.role = 'manager';
      this.closeDialog(temp);
    }).catch(err => {
      user.btnDisable = false;
      this.error(err);
    })
  }

  clearForm() {
    this.newUser = new User();
    this.newUser.role = 'manager';
  }

  setUpdate(u:User,temp){
    this.newUser = u;
    this.openDialog(temp,{'width':'320px'});
  }
}
