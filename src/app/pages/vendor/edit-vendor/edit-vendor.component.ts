import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Vendor, Bank } from 'src/app/models/vendor.model';
import { Model } from 'src/app/shared/common/contracts/model';
import { VendorService } from 'src/app/service/vendor.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { CropperService } from 'src/app/shared/components/image-cropper/cropper.service';
import { DialogService } from 'src/app/helperComponents/dialog/dialog.service';
import { FormControl } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { UserService } from 'src/app/service/user.service';
import { User } from 'src/app/models/user';
import { MessageService } from 'src/app/helperComponents/message/message.service';
import { DatePipe } from '@angular/common';
import { Language } from 'src/app/models/language.model';
import { Page } from 'src/app/shared/common/contracts/page';
import { LanguageService } from 'src/app/service/language.servcie';
declare var google;

@Component({
  selector: 'app-edit-vendor',
  templateUrl: './edit-vendor.component.html',
  styleUrls: ['./edit-vendor.component.css']
})
export class EditVendorComponent implements OnInit {

  temp: any = 'create';
  role = ['owner', 'manager', 'employee'];
  temps = [];
  vendor: Model<Vendor>;
  language:Page<Language>;
  user = new User();
  subscription: Subscription;
  id: any;
  selectImg: any;
  ratio: any;
  loading: boolean = true;
  @ViewChild('search')
  public serachElementRef: ElementRef
  searchControl: FormControl;
  geocoder: any;
  searchInputHidden:boolean = true;
  coordinates:Coordinates = new Coordinates();
  viewAddLanguage:boolean = false;

  admin = JSON.parse(window.localStorage.getItem('user'));

  constructor(
    private vendorService: VendorService,
    private activatedRoute: ActivatedRoute,
    public cropperService: CropperService,
    public dialogService: DialogService,
    private mapAPILoader: MapsAPILoader,
    private userService: UserService,
    private msgService: MessageService,
    private ngZone: NgZone,
    private datePipe:DatePipe,
    private languageService:LanguageService
  ) {
    this.vendor = new Model({
      api: vendorService.restaurants,
      properties: new Vendor()
    });

    this.language = new Page({
      api:languageService.languages,
      properties: new Language(),
      serverPaging:false,
    });

    this.subscription = this.activatedRoute.params.subscribe(param => {
      this.id = param.id;
    });

    if(this.id == 'new'){
      this.viewAddLanguage = true;
    }

    this.cropperService.setUploader();

    this.fetch(true);

  }

  error(value) {
    this.msgService.error(value);
  }

  success(value) {
    this.msgService.success(value);
  }

  ngOnInit() {
    this.searchControl = new FormControl();
    if(this.id == 'new'){this.setCurrentLocation();};
    this.searchLocation();
  }

  searchLocation(temp?: boolean) {
    if (temp) {
      this.temp = 'employees';
    }
    this.mapAPILoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder();
      let autocomplete = new google.maps.places.Autocomplete
        (this.serachElementRef.nativeElement, {
          types: ["address"]
        });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: any = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          let address = place.formatted_address;
          if(address){
            this.vendor.properties.location = address;
          }
          this.vendor.properties.coordinates = [place.geometry.location.lng(), place.geometry.location.lat()];
          this.coordinates.lat = this.vendor.properties.coordinates[1];
        this.coordinates.lng = this.vendor.properties.coordinates[0];
        this.coordinates.zoom = 15;
        });
      });
    });
  }

  setCords(lat, lng) {
    // if (this.id == 'new') {
    this.vendor.properties.coordinates = [lng, lat];
    this.geocoder.geocode({
      'location': {
        lat: lat,
        lng: lng
      }
    }, (results, status) => {
      if (results.length == 0) return false;
      this.vendor.properties.location = results[0].formatted_address;
    })
    // }
  }

  setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.coordinates.zoom = 15;
        this.coordinates.lat = position.coords.latitude
        this.coordinates.lng =position.coords.longitude
        this.vendor.properties.coordinates = [position.coords.longitude, position.coords.latitude];
      });
    }
  }

  fetch(loading: boolean) {
    this.loading = loading;
    if (this.id != 'new') {
      window.localStorage.setItem('restaurantId',this.id);
      this.vendor.fetch(this.id).then(() => {
        this.vendor.properties.languages = [];
        if(!this.vendor.properties.restaurantLanguages.length){
          this.viewAddLanguage = true;
        }
        this.vendor.properties.restaurantLanguages.forEach(i => {
          i.language.selected = true;
          this.vendor.properties.languages.push(i.language);
        });

        this.language.fetch().then((data) => {
          data.items.forEach(l => {
            l.selected = false;
            if(l.name.toLowerCase() =='english' ){
              l.id = null;
              l.selected = true;
            }
            if(this.vendor.properties.languages.length){
            let index = this.vendor.properties.languages.findIndex(e => e.id == l.id);
            if(index >= 0 ){
              l.selected = true;
            }
          }
          });
        });

        this.loading = false;
        this.vendor.properties.availFrom = this.datePipe.transform(this.vendor.properties.availFrom,'HH:mm');
        this.vendor.properties.availTo = this.datePipe.transform(this.vendor.properties.availTo,'HH:mm');
        if (!this.vendor.properties.bankDetail) {
          this.vendor.properties.bankDetail = new Bank();
        }
        this.coordinates.lat = this.vendor.properties.coordinates[1];
        this.coordinates.lng = this.vendor.properties.coordinates[0];
        this.coordinates.zoom = 15;
      }).catch(err => {
        this.error(err);
      });
    } else {
      this.loading = false;
      this.vendor.properties = new Vendor();
      this.vendor.properties.bankDetail = new Bank();
      this.user.role = 'owner';
    }
  }

  chnageTemp(temp) {
    this.temp = temp;
  }

  uploadImg(url, imgs: Array<any>, temp?: any) {
    this.cropperService.setUploderOptions({ url: url });
    this.cropperService.uploader.clearQueue();
      imgs.forEach(i => {
      if(i.file){
        this.cropperService.uploader.addToQueue([i.file]);
        }
      });

    if (this.cropperService.uploader.queue.length) {
      if (temp) {
        this.dialogService.openDialog(this.temps, temp, { 'background': 'transparent', 'padding': '5px' },true);
      }
    }
  }

  openMap(temp){
    this.vendor.properties.errLocation = null;
    this.dialogService.openDialog(this.temps, temp, { 'background': 'transparent','height':'100%','width':'100%' },true);
    this.searchInputHidden = false;
  }

  closeMap(temp){
    this.searchInputHidden = true;
    this.dialogService.close(temp,this.temps);
    this.vendor.properties.errLocation = null;
  }

  onSelectImg(event, temp, style, ratio?: any) {
    if (event.target.files.length) {
      this.selectImg = event;
      if (ratio) {
        this.ratio = ratio;
      }else{
        this.ratio = null;
      }
      this.dialogService.openDialog(this.temps, temp, style);
    }
  }

  getCropImg(res, temp) {

    this.dialogService.close(temp, this.temps);
    if (res.error) {
      return this.error(res.error);
    }
    if (this.ratio == 10 / 10) {
      return this.vendor.properties.imgUrl = res;
    }
    this.vendor.properties.restaurantAttachments.push(res);


  }

  changeDay(value){
    this.vendor.properties[value]  = !this.vendor.properties[value];
  }

  create(temp, imgProgress) {
    let vendor = this.vendor.properties;
    if (!vendor.name) {
      return vendor.errName = 'Name is require';
    }

    if (!vendor.title) {
      return vendor.errTitle = 'Title is require';
    }

    if (!vendor.availFrom) {
      return vendor.errAvailFrom = 'Openig time is require';
    }

    if (!vendor.availTo) {
      return vendor.errAvailTo = 'Closing time is require';
    }

    if (!vendor.description) {
      return vendor.errDescription = 'Description is require';
    }

    if (!vendor.location) {
      return vendor.errLocation = 'Location is require';
    }

    vendor.languageIds = this.language.items.filter(i => i.selected && i.id).map(i => i.id);

    vendor.btnDisable = true;
    if (vendor.imgUrl) {
      this.uploadImg('/api/images/upload', [vendor.imgUrl], imgProgress);
    } else {
      this.afterImgUpload(vendor, temp,imgProgress);
    }
    this.uploadImages(vendor,temp,imgProgress);
  }

  uploadImages(vendor:Vendor,temp,temp2?:any,attachment?:any){
    this.cropperService.uploadAll((data, err,complateAll) => {
      if (err) {
        this.dialogService.closeAll([]);
        this.vendor.properties.btnDisable = false;
        return this.error(err);
      }

      if(complateAll){
        this.vendor.properties.btnDisable = false;
        this.dialogService.closeAll([]);
        this.cropperService.uploader.clearQueue();
      }

      if (data.url) {
        vendor.imgUrl = data.url;
      }

      if(data.url && vendor.id){
        if(attachment.length){
          this.uploadImg('/api/restaurants/attachment/' + this.id, attachment,temp);
          this.uploadImages(null,null);
        }
        return  this.afterImgUploadUpdate(vendor);
      }
      if(data.url && !vendor.id){
        return this.afterImgUpload(vendor, temp,temp2);
      }

      if(data.attachmentUrl){
        this.vendor.properties.restaurantAttachments.push({imgUrl:data.attachmentUrl});
      }

    });
  }

  afterImgUpload(vendor: Vendor, temp,imgProgress) {
    let attachment =  this.vendor.properties.restaurantAttachments.filter(i=> i.file).map(i => i);
    this.vendor.properties.restaurantAttachments = [];
    
    this.vendorService.updateRestaurants.create(vendor).then((data) => {
      vendor.btnDisable = false;
      this.id = data.id;
      this.user.role = 'owner';
      if (attachment.length) {
        this.uploadImg('/api/restaurants/attachment/' + this.id, attachment,imgProgress);
        this.uploadImages(this.vendor.properties,null,imgProgress);
      }
      this.temp = temp;
      // this.fetch(false);
    }).catch(err => {
      vendor.btnDisable = false;
      this.error(err);
    });
  }

  createAdmin(temp) {
    let user = this.user;
    if (!user.name) {
      return user.errName = 'Name is require';
    }

    if (!user.username) {
      return user.errUserName = 'User Name is require';
    }

    if (!user.password) {
      return user.errPassword = 'Password is require';
    }
    user.restaurantId = this.id;
    user.btnDisable = true;
    this.userService.users.create(user).then(() => {
      this.temp = temp;
      user.btnDisable = false;
      this.user = new User();
    }).catch(err => {
      user.btnDisable = false;
      this.error(err);
    })
  }

  newUser() {
    this.user = new User();
  }

  updateAdmin() {
    let user = this.user;
    user.restaurantId = this.id;
    user.btnDisable = true;
    if(user.password){
    let  data = {
        newPassword: this.user.password
      }
      this.userService.users.update(user.id,data,null,'updatePassword/'+user.id).then(() => {
        user.password =null;
        this.userService.users.update(user.id, user).then(() => {
          user.btnDisable = false;
          this.dialogService.closeAll([]);
        }).catch(err => {
          user.btnDisable = false;
          this.error(err);
        })
      }).catch(err => {
        this.error(err);
      });
    }else{
      this.userService.users.update(user.id, user).then(() => {
        user.btnDisable = false;
        this.dialogService.closeAll([]);
      }).catch(err => {
        user.btnDisable = false;
        this.error(err);
      })
    }
    
  }

  addBankDetails() {
    let bank = this.vendor.properties.bankDetail;
    if (!bank.bankName) {
      return bank.errBankName = 'Bank Name is require';
    }
    if (!bank.holderName) {
      return bank.errHolderName = 'Holder Name is require';
    }
    if (!bank.accountNo) {
      return bank.errAccountNo = 'Account No is require';
    }
    if (!bank.ifscCode) {
      return bank.errIfscCode = 'IFSC Code is require';
    }
    bank.restaurantId = this.id;
    bank.btnDisable = true;
    this.vendorService.bankDetails.create(bank).then(() => {
      this.success('add bank details');
      this.fetch(false);
      bank.btnDisable = false;
    }).catch(err => {
      bank.btnDisable = false;
      this.error(err);
    })
  }

  updateBankDetails() {
    let bank = this.vendor.properties.bankDetail;
    bank.restaurantId = this.id;
    bank.btnDisable = true;
    this.vendorService.bankDetails.update(bank.id, bank).then(() => {
      this.success('update bank details');
      this.fetch(false);
      bank.btnDisable = false;
    }).catch(err => {
      this.error(err);
      bank.btnDisable = false;
    })
  }

  update(temp) {
    let vendor = this.vendor.properties;
    this.vendor.properties.btnDisable = true;
    let attachment =  this.vendor.properties.restaurantAttachments.filter(i=> i.file).map(i => i);

    if (this.vendor.properties.imgUrl.file) {
      this.uploadImg('/api/images/upload',[this.vendor.properties.imgUrl], temp);
    }

    if(attachment.length && !this.vendor.properties.imgUrl.file){
      this.uploadImg('/api/restaurants/attachment/' + this.id, attachment,temp);
    }

    if(!attachment.length && !this.vendor.properties.imgUrl.file){
     return this.afterImgUploadUpdate(vendor);
    }
    this.uploadImages(vendor,temp,null,attachment);

  }

  afterImgUploadUpdate(vendor: Vendor) {
    vendor.languageIds = this.language.items.filter(i => i.selected && i.id).map(i => i.id);
    if(vendor.languageIds.length){
      this.viewAddLanguage = false;
    }
   delete vendor.restaurantAttachments;
    this.vendorService.updateRestaurants.update(this.vendor.properties.id, this.vendor.properties).then(() => {
      this.fetch(false);
      this.vendor.properties.btnDisable = false;
      this.success('update');
    }).catch(err => {
      this.vendor.properties.btnDisable = false;
      this.error(err);
    })
  }

  deleteAttachment(id){
    this.vendorService.deleteAttachments.remove(id).then(() => {
    this.fetch(false);
    this.success('delete attachment');
    }).catch(err => this.error(err));
    
  }

}

class Coordinates {
  lat?:number;
  lng?:number;
  zoom?:number = 3;
}
