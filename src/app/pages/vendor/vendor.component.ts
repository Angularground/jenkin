import { Component, OnInit } from '@angular/core';
import { Page } from 'src/app/shared/common/contracts/page';
import { VendorService } from 'src/app/service/vendor.service';
import { Vendor } from 'src/app/models/vendor.model';
import { MessageService } from 'src/app/helperComponents/message/message.service';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit {

  restaurants:Page<Vendor>;
  loading:boolean = true;

  status = [{
    label:'active',
    value:'active'
  },{
    label:'blocked',
    value:'inactive'
  },{
    label:'deteled',
    value:'deteled'
  }];

  constructor(
    private vendorService:VendorService,
    private msgService:MessageService
  ) {
    this.restaurants = new Page({
      api:vendorService.restaurants,
      properties: new Vendor,
      filters:[{
        field:'name',
        value:null
      },{
        field:'status',
        value:'active'
      }]
    });

    this.fetch(true);

   }

  ngOnInit() {
  }

  error(value) {
    this.msgService.error(value);
  }

  success(value) {
    this.msgService.success(value);
  }

  updateStatus(restaurant:Vendor,value){
    restaurant.status = value;
    this.update(restaurant,value);
  }


  fetch(loading){
    this.loading = loading;

    this.restaurants.fetch().then(() => {
      this.loading = false;
      this.restaurants.items.forEach(element => {
        if(element.rating){
          element.rating = parseInt(element.rating);
        }else{
          element.rating = 0;
        }
      });
    }).catch((err) => {
      
    });

  }

  delete(id){
    this.vendorService.updateRestaurants.remove(id).then(() =>{
      this.success('delete');
      this.fetch(false);
    }).catch((err) => {
      this.error(err);
    })
  }

  update(restaurant:Vendor,msg){
    this.vendorService.updateRestaurants.update(restaurant.id,restaurant).then(() =>{
      this.success(msg);
      this.fetch(false);
    }).catch(err =>{
      this.error(err);
    })
  }

}
