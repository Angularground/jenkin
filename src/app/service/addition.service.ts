import { Injectable } from "@angular/core";
import { IApi } from '../shared/common/contracts/api';
import { HttpClient } from '@angular/common/http';
import { GenericApi } from '../shared/common/generic-api';
import { Addition } from '../models/addition.model';
import { SizeVariant } from '../models/menu.model';

@Injectable({
    providedIn:'root'
})

export class AdditionService {

    additions:IApi<Addition>;
    varients:IApi<SizeVariant>;
    additionsForLanguage:IApi<Addition>;
    additionVarientsForLanguage:IApi<SizeVariant>;

    constructor(http: HttpClient) {
        this.additions = new GenericApi<Addition>('additions', http);
        this.varients = new GenericApi<Addition>('additionVariants', http);
        this.additionsForLanguage = new GenericApi<Addition>('additions/for/languages', http);
        this.additionVarientsForLanguage = new GenericApi<Addition>('additionVariants/for/languages', http);
      }


}