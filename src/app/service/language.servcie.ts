import { Injectable } from "@angular/core";
import { IApi } from '../shared/common/contracts/api';
import { HttpClient } from '@angular/common/http';
import { GenericApi } from '../shared/common/generic-api';
import { Language } from '../models/language.model';

@Injectable({
    providedIn:'root'
})

export class LanguageService {

    languages:IApi<Language>;
    menuLanguages:IApi<any>;
    itemLanguages:IApi<any>;
    sizeVariantLanguages:IApi<any>;
    additionLanguages:IApi<any>;
    additionVariantLanguages:IApi<any>;

    constructor(http: HttpClient) {
        this.languages = new GenericApi<Language>('languages', http);
        this.menuLanguages = new GenericApi<any>('menuLanguages', http);
        this.itemLanguages = new GenericApi<any>('itemLanguages', http);
        this.sizeVariantLanguages = new GenericApi<any>('sizeVariantLanguages', http);
        this.additionLanguages = new GenericApi<any>('additionLanguages', http);
        this.additionVariantLanguages = new GenericApi<any>('additionVariantLanguages', http);
      }


}