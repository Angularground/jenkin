import { Injectable } from "@angular/core";
import { IApi } from '../shared/common/contracts/api';
import { HttpClient } from '@angular/common/http';
import { GenericApi } from '../shared/common/generic-api';
import { Menu, Item, SizeVariant } from '../models/menu.model';

@Injectable({
    providedIn:'root'
})

export class MenuService {

    menus:IApi<Menu>;
    languageGet:IApi<Menu>;
    items:IApi<Item>;
    itemsForLanguage:IApi<Item>;
    sizeVariants:IApi<SizeVariant>;
    sizeVariantsForLanguage:IApi<SizeVariant>;


    constructor(http: HttpClient) {
        this.menus = new GenericApi<Menu>('menus', http);
        this.languageGet = new GenericApi<Menu>('menus/for/languages', http);
        this.items = new GenericApi<Item>('items', http);
        this.itemsForLanguage = new GenericApi<Item>('items/for/languages', http);
        this.sizeVariants = new GenericApi<SizeVariant>('sizeVariants', http);
        this.sizeVariantsForLanguage = new GenericApi<SizeVariant>('sizeVariants/for/languages', http);
      }


}