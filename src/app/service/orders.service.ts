import { Injectable } from "@angular/core";
import { IApi } from '../shared/common/contracts/api';
import { HttpClient } from '@angular/common/http';
import { GenericApi } from '../shared/common/generic-api';
import { Order } from '../models/order.model';

@Injectable({
    providedIn:'root'
})

export class OrderService {

    orders:IApi<Order>;
    ordersById:IApi<Order>;


    constructor(http: HttpClient) {
        this.orders = new GenericApi<Order>('orders/for/panel', http);
        this.ordersById = new GenericApi<Order>('orders', http);
      }


}