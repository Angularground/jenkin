import { Injectable } from "@angular/core";
import { IApi } from '../shared/common/contracts/api';
import { HttpClient } from '@angular/common/http';
import { GenericApi } from '../shared/common/generic-api';
import { Requests } from '../models/request.model';

@Injectable({
    providedIn:'root'
})

export class RequestService {

    requests:IApi<Requests>;


    constructor(http: HttpClient) {
        this.requests = new GenericApi<Requests>('requests', http);
      }


}