import { Injectable } from "@angular/core";
import { IApi } from '../shared/common/contracts/api';
import { HttpClient } from '@angular/common/http';
import { GenericApi } from '../shared/common/generic-api';

@Injectable({
    providedIn:'root'
})

export class UserService {

    users:IApi<any>;
    signIn:IApi<any>;
    constructor(http: HttpClient) {
        this.signIn = new GenericApi<any>('users/signin', http);
        this.users = new GenericApi<any>('users', http);
      }


}