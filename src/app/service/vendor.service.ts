import { Injectable } from "@angular/core";
import { IApi } from '../shared/common/contracts/api';
import { HttpClient } from '@angular/common/http';
import { GenericApi } from '../shared/common/generic-api';
import { Vendor, Bank } from '../models/vendor.model';

@Injectable({
    providedIn:'root'
})

export class VendorService {

    restaurants:IApi<Vendor>;
    updateRestaurants:IApi<Vendor>;
    bankDetails:IApi<Bank>;
    deleteAttachments:IApi<any>;
    constructor(http: HttpClient) {
        this.restaurants = new GenericApi<Vendor>('restaurants/for/panel', http);
        this.updateRestaurants = new GenericApi<Vendor>('restaurants', http);
        this.deleteAttachments = new GenericApi<any>('restaurants/del/attachment', http);
        this.bankDetails = new GenericApi<Bank>('bankDetails', http);
      }


}