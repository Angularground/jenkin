import { Component, OnInit, EventEmitter, AfterViewInit, Input, Output } from '@angular/core';
import Cropper from 'cropperjs';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { CropperService } from '../cropper.service';
// import { CropperService } from '../cropper.service';


@Component({
  selector: 'app-cropper',
  templateUrl: './cropper.component.html',
  styleUrls: ['./cropper.component.css']
})
export class CropperComponent implements OnInit, AfterViewInit {

  onClose: EventEmitter<any> = new EventEmitter();
  cropper: Cropper;
  file: File;
  processing: boolean;
  isError: boolean = false;
  @Input() imageChangedEvent: any = '';
  @Input() ratio: any = null ;
  @Output() url: EventEmitter<any> = new EventEmitter();
  croppedImage: any = '';

  constructor() { 
  }

  ngOnInit() {
    if (!this.file) {
      this.isError = true;
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event;
  }

  selectImg(error?: string) {
    let res = {
      error:null,
      file:null,
      base64:null
    }
    if (error) {
      res.error = error;
      return this.url.emit(res);
    }
    this.getImageFromBase64(this.croppedImage.file as any, this.croppedImage.base64, (file) => {
      this.croppedImage.file = file;
      res.file = file;
      res.base64 = this.croppedImage.base64;
      this.url.emit(res);
    });
  }

  onCancel() {
    this.url.emit(null);
  }



  // ngAfterViewInit() {
  //   var image = document.querySelector('#image') as HTMLImageElement;
  //   this.cropper = new Cropper(image, {
  //     movable: false,
  //     zoomable: false,
  //     rotatable: false,
  //     scalable: false
  //   });
  // }

  close(file?: File) {
    this.onClose.emit(file);
  }

  save() {
    // this.cropperService.imageCropper(this.file).close();
    let result = this.cropper.getCroppedCanvas({ width: 300, height: 300 });
    this.getImageFromBase64(this.file, result.toDataURL('image/jpeg', 0.8), (file) => {
      this.close(file);
    });
  }

  ngAfterViewInit() {
    this.dataUrl(this.file, (err, base64) => {
      document.getElementById('image')['src'] = base64;
      this.initCropper();
    });
  }


  private initCropper() {
    // const image: HTMLImageElement = document.getElementById('image') as HTMLImageElement;
    // const img = new Image();
    const image = document.querySelector('#image') as HTMLImageElement;


    // img.onload = () => {
    this.cropper = new Cropper(image, {
      // dragMode: Cropper.DragMode.Crop,
      aspectRatio: 500 / 500, // for free crop
      autoCrop: true,
      autoCropArea: 0.9,
      restore: false,
      guides: true,
      center: true,
      responsive: true,
      highlight: false,
      cropBoxMovable: true,
      cropBoxResizable: false,
      toggleDragModeOnDblclick: false,
    });
    // };
    // img.src = image['src'];
  }

  private dataUrl(file: File, callback) {
    const reader = new FileReader();
    reader.onload = function (e) {
      const target: any = e.target;
      callback(null, target.result);
    };
    // reader.readAsDataURL(file);
  }

  private getImageFromBase64(file: File, base64, callback) {
    let dataURI = base64;
    let typeOfImage = file.type;
    let nameOfImage = file.name;
    // convert base64 to raw binary data held in a string
    let byteString = atob(dataURI.split(',')[1]);
    // separate out the mime component
    let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
    // write the bytes of the string to an ArrayBuffer
    let ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    // write the ArrayBuffer to a blob, and you're done
    let bb = new Blob([ab], { type: 'image/jpeg' });
    let editedFile: any;

    try {
      editedFile = new File([bb], this.imageChangedEvent.target.files[0].name, { type: 'image/jpeg' });
    } catch (err) {
      editedFile = bb;
      editedFile.name = this.imageChangedEvent.target.files[0].name;
      editedFile.lastModifiedDate = new Date();
    }
    return callback(editedFile);
  }

}
