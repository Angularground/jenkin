export const environment = {
  production: true,
  apiUrls: {
    api: 'https://wopadu.bcodercastle.com'
    // api:'http://localhost:3300'
  },
  name:'prod'
};
